import {html, render} from "./node_modules/lit-html/lit-html.js";


const cadena = ">>>>>>>>>>Contenido dinamico<<<<<<<<<<";



const templateHolder = (paramString) => html`<h2>Contenido estatico (template) + ${paramString} </h2>`;
const objectTemplateResult = templateHolder(cadena); 


render(  templateHolder(cadena), document.getElementById('container1') );
render(  templateHolder('oootttrrrooo ttteeexxxtttooo.'), document.getElementById('container2'));